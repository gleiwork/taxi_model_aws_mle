import pytest
import flask
import json

import random
import mlflow

class DummyModel():
    def predict(self,X):
        return [random.uniform(0.0001, 10)+1]

@pytest.fixture
def client(mocker):
    def mock_load_model(model_uri=None):
        return DummyModel()

    mocker.patch('mlflow.pyfunc.load_model',mock_load_model)

    from taxi_model_aws_mle.model_api import api

    client = api.app.test_client()
    yield client

def test_modprv_info_avance_tramite(client):
    
    request = [{"feature":"vendor_id","value":1},
                {"feature":"pickup_datetime","value":"2016-06-30 23:59:58"},
                {"feature":"passenger_count","value":1},
                {"feature":"pickup_longitude","value":-73.988128662109375},
                {"feature":"pickup_latitude","value":40.732028961181641},
                {"feature":"dropoff_longitude","value":-73.99017333984375},
                {"feature":"dropoff_latitude","value":40.756679534912109},
                {"feature":"store_and_fwd_flag","value":"N"}]

    rv = client.post('/taxi_trip_duration_inference', data=json.dumps(request),
                     content_type='application/json')
    response = json.loads(rv.data)
    assert rv.status_code == 200
    assert len(response)==10
    assert 'trip_duration' in response
    assert isinstance(response['trip_duration'],float)
