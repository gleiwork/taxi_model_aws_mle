import pandas as pd
from taxi_model_aws_mle.column_transformers import column_transformers

def test_haversine_array():
    lat1 = 40.7320
    lng1 = -73.9881
    lat2 = 40.7566
    lng2 = -73.9901
    distance = column_transformers.haversine_array(lat1, lng1, lat2, lng2)
    assert distance==2.7405794071113037
    assert isinstance(distance,float)

def test_dummy_manhattan_distance():
    lat1 = 40.7320
    lng1 = -73.9881
    lat2 = 40.7566
    lng2 = -73.9901
    distance = column_transformers.dummy_manhattan_distance(lat1, lng1, lat2, lng2)
    assert distance == 2.9039155584754526
    assert isinstance(distance,float)

def test_bearing_array():
    lat1 = 40.7320
    lng1 = -73.9881
    lat2 = 40.7566
    lng2 = -73.9901
    distance = column_transformers.bearing_array(lat1, lng1, lat2, lng2)
    assert distance == -3.524080896395574
    assert isinstance(distance,float)

def test_haversine_array_transformer():
    X = [{'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901},
         {'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901}]
    X = pd.DataFrame(X)
    X_in_len = len(X.columns)
    X = column_transformers.haversine_array_transformer(X)
    print(X)
    assert len(X.columns)==X_in_len+1
    assert 'distance_haversine' in X.columns

def test_dummy_manhattan_distance_transformer():
    X = [{'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901},
         {'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901}]
    X = pd.DataFrame(X)
    X_in_len = len(X.columns)
    X = column_transformers.dummy_manhattan_distance_transformer(X)
    print(X)
    assert len(X.columns)==X_in_len+1
    assert 'distance_dummy_manhattan' in X.columns


def test_haversine_array_transformer():
    X = [{'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901},
         {'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901}]
    X = pd.DataFrame(X)
    X_in_len = len(X.columns)
    X = column_transformers.haversine_array_transformer(X)
    print(X)
    assert len(X.columns)==X_in_len+1
    assert 'distance_haversine' in X.columns

def test_dates_hours_transform():
    X = [{'pickup_datetime':"2016-06-30 23:59:58",'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901},
         {'pickup_datetime':"2016-06-30 23:59:58",'pickup_latitude':40.7320, 'pickup_longitude':-73.9881,'dropoff_latitude':40.7566,'dropoff_longitude':-73.9901}]
    X = pd.DataFrame(X)
    X_in_len = len(X.columns)
    X = column_transformers.dates_hours_transform(X)
    print(X)
    assert len(X.columns)==X_in_len+4
    assert 'DayofMonth' in X.columns
    assert 'Month' in X.columns
    assert 'Hour' in X.columns
    assert 'dayofweek' in X.columns
    


    