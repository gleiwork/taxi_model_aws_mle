prepare_data:
	python -m taxi_model_aws_mle.prepare_data.data_manager --dataset-uri s3://awsmle.data/raw/data.csv \
                --dataset-version 0.7

train_model:
	export mlflow_url=http://Deplo-MLFLO-D88OEVSY616U-82eac8269ee913f7.elb.us-east-1.amazonaws.com
	python -m taxi_model_aws_mle.train_model.train_model --data-uri s3://awsmle.data/transformed/v0.7/ \
                --model-name taxi_model_aws

model_api:
	export FLASK_IP=127.0.0.1
	export MODEL_STAGE=Staging
	python -m taxi_model_aws_mle.model_api.api

unit_tests:
	pytest -v -o junit_family=xunit1  --cov taxi_model_aws_mle --junitxml=./results.xml

install:
	python -m pip install .[tests]

build:
	python setup.py bdist_wheel

