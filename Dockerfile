FROM python:3.8

COPY dist/model_api*.whl ./

RUN python -m pip install model_api*.whl

CMD ["python","-m","model_api.api"]

EXPOSE 5000