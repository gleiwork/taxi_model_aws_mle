from setuptools import find_packages, setup

test_requirements = ['pytest','pytest-mock','pytest-cov' ]
description = """A taxi company wants to improve the user experience of their 
                 app and is requesting AWS Professional Services to build a solution 
                 that will be integrated with their app. The solution should be able 
                 to predict how long the cab ride will take."""

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name='taxi_model_aws_mle',
    packages=find_packages(),
    version='0.1.0',
    description=description,
    author='Gleiwer Montoya',
    license='MIT',
    install_requires=requirements,
    extras_require={
        "tests": test_requirements,
    },
)
