import os
from sklearn.model_selection import train_test_split
import logging
import pandas as pd
import argparse
import mlflow
import tempfile
import boto3
import io
from taxi_model_aws_mle.storage_connection import storage_connection
import numpy as np

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class TaxiDataLoader:

    def __init__(self,uri,dataset_version):
        self.s3_client = storage_connection.StorageConnection().storage_client

        self.tmp_data_path = tempfile.NamedTemporaryFile(mode = 'w+b',delete=False)
        self.data = self.get_data(uri)
        self.data = self.trim_dataset(self.data)
        self.dataset_version = dataset_version
    
    def split_data(self,**kwargs):
        logging.info("Splitting data into train and test...")
        train, test = train_test_split(self.data,**kwargs)
        
        with io.BytesIO() as pickle_buffer:
            train.to_pickle(pickle_buffer)
            logging.info("Saving train dataset in storage bucket...")
            self.s3_client.put_object(Body=pickle_buffer.getvalue(), 
                                      Bucket=self.bucket_name, 
                                      Key='transformed/v{data_version}/train.pkl'.format(data_version=self.dataset_version))
            logging.info("Saving test dataset in storage bucket...")
            test.to_csv(pickle_buffer)
            self.s3_client.put_object(Body=pickle_buffer.getvalue(), 
                                      Bucket=self.bucket_name, 
                                      Key='transformed/v{data_version}/test.pkl'.format(data_version=self.dataset_version))


        return 's3://{}/v{}'.format(self.bucket_name, self.dataset_version)

    def trim_dataset(self,dataset):

        #exclude two standard deviations from mean
        logging.info("Removing outliers from objective variable...")
        m = np.mean(dataset['trip_duration'])
        s = np.std(dataset['trip_duration'])
        dataset = dataset[dataset['trip_duration'] <= m + 2*s]
        dataset = dataset[dataset['trip_duration'] >= m - 2*s]

        #leave locations outside NY
        logging.info("Removing locations outside New York...")
        dataset = dataset[dataset['pickup_longitude'] <= -73.75]
        dataset = dataset[dataset['pickup_longitude'] >= -74.03]
        dataset = dataset[dataset['pickup_latitude'] <= 40.85]
        dataset = dataset[dataset['pickup_latitude'] >= 40.63]
        dataset = dataset[dataset['dropoff_longitude'] <= -73.75]
        dataset = dataset[dataset['dropoff_longitude'] >= -74.03]
        dataset = dataset[dataset['dropoff_latitude'] <= 40.85]
        dataset = dataset[dataset['dropoff_latitude'] >= 40.63]

        return dataset

    def get_data(self,uri):
        from urllib.parse import urlparse

        o = urlparse(uri)
        self.bucket_name = o.netloc
        path = o.path[1:]
        logging.info("Getting data from bucket...") 
        response = self.s3_client.get_object(Bucket=self.bucket_name,Key=path)
        data = pd.read_csv(response.get("Body"))
        return data

        
    def clean_data(self):
        # exclude 2 standard deviations from the mean

        self.data['log_trip_duration'] = np.log(self.data['trip_duration'].values + 1)

        x_fields = ['vendor_id',
                    'pickup_datetime',
                    'passenger_count',
                    'pickup_longitude',
                    'pickup_latitude',
                    'dropoff_longitude','dropoff_latitude',
                    'store_and_fwd_flag',
                    'log_trip_duration']

        
        self.data = self.data[x_fields]



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--dataset-version', dest='dataset_version', help='dataset version')
    parser.add_argument('--dataset-uri', dest='dataset_uri', help='dataset version')
    args = parser.parse_args()

    data_manager = TaxiDataLoader(args.dataset_uri,args.dataset_version)
    data_manager.clean_data()
    data_path = data_manager.split_data(test_size=0.7,train_size=0.3)
      

if __name__ == '__main__':
    main()
