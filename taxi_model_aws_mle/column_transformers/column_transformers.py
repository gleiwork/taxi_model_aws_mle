from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.cluster import MiniBatchKMeans
import numpy as np
import pandas as pd

def haversine_array_transformer(X):
    X.loc[:, 'distance_haversine'] = haversine_array(X['pickup_latitude'].values, 
                            X['pickup_longitude'].values, 
                            X['dropoff_latitude'].values, 
                            X['dropoff_longitude'].values)
    return X

def dummy_manhattan_distance_transformer(X):
    X.loc[:, 'distance_dummy_manhattan'] =  dummy_manhattan_distance(X['pickup_latitude'].values, 
                                                    X['dropoff_longitude'].values,
                                                    X['dropoff_latitude'].values, 
                                                    X['dropoff_longitude'].values)
    return X

def bearing_array_transformer(X):
    X.loc[:, 'direction'] = bearing_array(X['pickup_latitude'].values, 
                                    X['pickup_longitude'].values, 
                                    X['dropoff_latitude'].values, 
                                    X['dropoff_longitude'].values)
    return X

def clusters_transformer(X):
    coords = np.vstack((X[['pickup_latitude', 'pickup_longitude']].values,
                    X[['dropoff_latitude', 'dropoff_longitude']].values))
    sample_ind = np.random.permutation(len(coords))[:500000]
    kmeans = MiniBatchKMeans(n_clusters=100, batch_size=10000).fit(coords[sample_ind])
    X.loc[:, 'pickup_cluster'] = kmeans.predict(X[['pickup_latitude', 'pickup_longitude']])
    X.loc[:, 'dropoff_cluster'] = kmeans.predict(X[['dropoff_latitude', 'dropoff_longitude']])
    return X

def dates_hours_transform(X):
    X['pickup_datetime'] = pd.to_datetime(X.pickup_datetime)
    X['DayofMonth'] = X['pickup_datetime'].dt.day
    X['Month'] = X['pickup_datetime'].dt.month
    X['Hour'] = X['pickup_datetime'].dt.hour
    X['dayofweek'] = X['pickup_datetime'].dt.dayofweek
    return X


def haversine_array(lat1, lng1, lat2, lng2):
    lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
    AVG_EARTH_RADIUS = 6371  # in km
    lat = lat2 - lat1
    lng = lng2 - lng1
    d = np.sin(lat * 0.5) ** 2 + np.cos(lat1) * np.cos(lat2) * np.sin(lng * 0.5) ** 2
    h = 2 * AVG_EARTH_RADIUS * np.arcsin(np.sqrt(d))
    return h

def dummy_manhattan_distance(lat1, lng1, lat2, lng2):
    a = haversine_array(lat1, lng1, lat1, lng2)
    b = haversine_array(lat1, lng1, lat2, lng1)
    return a + b

def bearing_array(lat1, lng1, lat2, lng2):
    AVG_EARTH_RADIUS = 6371  # in km
    lng_delta_rad = np.radians(lng2 - lng1)
    lat1, lng1, lat2, lng2 = map(np.radians, (lat1, lng1, lat2, lng2))
    y = np.sin(lng_delta_rad) * np.cos(lat2)
    x = np.cos(lat1) * np.sin(lat2) - np.sin(lat1) * np.cos(lat2) * np.cos(lng_delta_rad)
    return np.degrees(np.arctan2(y, x))
