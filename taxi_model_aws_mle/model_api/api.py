#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
app.py: Api para servir modelos de calculo de tiempo del tracking previsional
"""

from flask import Flask,abort,request,jsonify,json
from werkzeug.exceptions import HTTPException
from datetime import datetime

import os
import traceback
import sys
import secrets
import string

import logging

import mlflow.pyfunc
import os
import pandas as pd
import numpy as np

__author__ = "Gleiwer Montoya"
__copyright__ = "Copyright (c) 2020"
__credits__ = [__author__,]
__license__ = "MIT"
__maintainer__ = __author__
__email__ = "gleiwer.montoya@proteccion.com.co"
__status__ = "Beta"

mlflow.set_tracking_uri(os.getenv('mlflow_url'))

environment = os.getenv('MODEL_STAGE')

model = mlflow.pyfunc.load_model(
    model_uri="models:/{model_name}/{stage}".format(model_name='taxi_model_aws',stage=environment)
)

features = ["vendor_id",
          "passenger_count",
          "pickup_longitude",
          "pickup_latitude",
          "dropoff_longitude",
          "dropoff_latitude"]

import watchtower, logging
logging.basicConfig(level=logging.INFO)
logger = logging.getLogger("taxi_model_aws")
logger.addHandler(watchtower.CloudWatchLogHandler())


app = Flask(__name__)

def inference_id(**args): 
    time = datetime.now()
    inference_id = time.strftime("%Y%m%d-%H%M%S")+'-{}'.format(''.join(secrets.choice(string.digits) for i in range(4)))
    
    return {"inference_id": inference_id}


@app.route('/taxi_trip_duration_inference', methods=['POST'])
def tiempo_normalizacion():
    """Recibe el detalle y el tema de un texto de PQR"""
    input = request.get_json()
    try:
        x_dict = {i['feature']:i['value'] for i in input}
        x_list = [[x_dict[key] for key in features]]

        log_result = model.predict(pd.DataFrame(x_dict,index=[0]))
        resultado = np.exp(log_result[0]) - 1
        data = inference_id()
        
        data.update(x_dict)
        inference = {"trip_duration":resultado} 
        data.update(inference)
        return jsonify(data)

    except RuntimeError as e:
        abort(500,repr(e))


@app.after_request
def after_request(response):
    logger.info(response.json)
    return response

if __name__ == '__main__':
    app.config['RESTPLUS_MASK_SWAGGER'] = False
    certs_path = os.environ.get('CERTS_PATH')
    try:
        app.run(debug=False, host=os.environ.get('FLASK_IP'))#,ssl_context=(os.path.join(certs_path,'tls.crt'), os.path.join(certs_path,'tls.key')))
    except OSError as e:
        traceback.print_exc(file=sys.stdout)
        print(e.filename)

