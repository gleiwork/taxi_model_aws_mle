
import numpy as np 
from sklearn.metrics import mean_absolute_error
from sklearn.metrics import mean_squared_error
from sklearn.metrics import r2_score
from sklearn.linear_model import ElasticNet
import os
import pandas as pd
import logging
from taxi_model_aws_mle.storage_connection import storage_connection
import pickle
from taxi_model_aws_mle.column_transformers import column_transformers as ct
from sklearn.preprocessing import FunctionTransformer
from sklearn.compose import ColumnTransformer
from sklearn.pipeline import Pipeline
from sklearn.ensemble import GradientBoostingRegressor
from sklearn.pipeline import make_pipeline
from sklearn.preprocessing import OneHotEncoder

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class TaxiModel:

    def __init__(self,data_uri):
        self.data_uri=data_uri
        self.params = {'n_estimators':100,
                  'learning_rate':0.025,
                  'max_depth':5
                  }
        self.y_field = 'trip_duration'
        self.model = self.get_model()
        self.s3_client = storage_connection.StorageConnection().storage_client

        
    def get_model(self):
        logging.info("Building model definition...")
        distances_pipeline = make_pipeline(FunctionTransformer(ct.haversine_array_transformer,validate=False),
                                           FunctionTransformer(ct.dummy_manhattan_distance_transformer,validate=False),
                                           FunctionTransformer(ct.bearing_array_transformer,validate=False),
                                           FunctionTransformer(ct.dates_hours_transform,validate=False),
                                           )

        preprocessor = ColumnTransformer(transformers=[
            ('One Hot Encoders',OneHotEncoder(),['vendor_id',
                                                 'passenger_count',
                                                 'store_and_fwd_flag',
                                                 'Month',
                                                 'DayofMonth',
                                                 'Hour',
                                                 'dayofweek']),
        ])

        classifier = GradientBoostingRegressor(n_estimators=self.params['n_estimators'],
                                              random_state=0,
                                              learning_rate=self.params['learning_rate'],
                                              max_depth=self.params['max_depth'])

        return Pipeline(steps=[('new_features',distances_pipeline),
                             ('preprocessor', preprocessor),
                             ('classifier', classifier)])

    def eval_metrics(self, actual, pred):
        rmse = np.sqrt(mean_squared_error(actual, pred))
        mae = mean_absolute_error(actual, pred)
        r2 = r2_score(actual, pred)
        logging.info("Evaluation metrics rmse:{rmse}, mae:{mae}, r2:{r2}".format(rmse=rmse,mae=mae,r2=r2))
        self.metrics = {'rmse':rmse,
                        'mae':mae,
                        'r2':r2}

    def evaluation(self,test_x,test_y):
        predicted_values = self.model.predict(test_x)
        self.eval_metrics(test_y,predicted_values)

    def fit(self,train_x,train_y):
        logging.info("Training model...")
        self.model.fit(train_x, train_y)
        return self

    def load_file(self,uri):
        from urllib.parse import urlparse

        o = urlparse(uri)
        self.bucket_name = o.netloc
        path = o.path[1:]
       
        response = self.s3_client.get_object(Bucket=self.bucket_name,Key=path)
        body = response['Body'].read()
        data = pickle.loads(body)
        return data

    def get_datasets(self,y_field):
        train_file = '/'.join(s.strip('/') for s in [self.data_uri,'train.pkl'])
        test_file = '/'.join(s.strip('/') for s in [self.data_uri,'test.pkl'])

        try:
            logging.info("Getting train data from bucket...")
            train = self.load_file(train_file)
            logging.info("Getting test data from bucket...")

            test = self.load_file(test_file)
        except Exception as e:
            logger.exception(
                "Unable to download training & test files, check your internet connection. Error: %s", e
            )

        x_train = train.drop([y_field],axis=1)
        x_test = test.drop([y_field],axis=1)
        y_train = train[y_field]
        y_test = test[y_field]

        return x_train,y_train,x_test,y_test

    