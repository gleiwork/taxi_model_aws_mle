import os
import warnings
import sys

import pandas as pd
import numpy as np
from sklearn.metrics import mean_squared_error, mean_absolute_error, r2_score
from sklearn.model_selection import train_test_split
from sklearn.linear_model import ElasticNet
from urllib.parse import urlparse
import mlflow
import mlflow.sklearn
from taxi_model_aws_mle.train_model.taxi_model import TaxiModel
import logging
import argparse

logging.basicConfig(level=logging.WARN)
logger = logging.getLogger(__name__)

def get_file_path(data_uri):
    train_path = os.path.join(data_uri,'train.csv')
    test_path = os.path.join(data_uri,'test.csv')
    return train_path,test_path

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--data-uri',dest='data_uri',help='train and test dataset paths')
    parser.add_argument('--model-name',dest='model_name',help='Mlflow registry name')
    args = parser.parse_args()
    mlflow_run(args.data_uri,args.model_name)

def mlflow_run(data_uri,model_name):
    warnings.filterwarnings("ignore")
    np.random.seed(40)

    taxi_model = TaxiModel(data_uri)
    mlflow.set_tracking_uri(os.getenv('mlflow_url'))
    mlflow.set_experiment("Taxi Model AWS - MLE")

    with mlflow.start_run():
        
        train_x,train_y,test_x,test_y = taxi_model.get_datasets(y_field='log_trip_duration')
        taxi_model.fit(train_x,train_y)
        taxi_model.evaluation(test_x,test_y)

        for param in taxi_model.params:
            mlflow.log_param(param, taxi_model.params[param])
        
        for metric in taxi_model.metrics:
            mlflow.log_metric(metric,taxi_model.metrics[metric])

        tracking_url_type_store = urlparse(mlflow.get_tracking_uri()).scheme

        # Model registry does not work with file store
        if tracking_url_type_store != "file":
            mlflow.sklearn.log_model(taxi_model.model, "model", registered_model_name=model_name)
        else:
            mlflow.sklearn.log_model(taxi_model.model, "model")


if __name__ == "__main__":
    main()