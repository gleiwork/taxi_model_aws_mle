taxi_model_aws_mle
================================================
by: Gleiwer Andrés Montoya Mosquera<p>
gleiwork@gmail.com


Problem: A taxi company wants to improve the user experience of their app and is requesting AWS Professional Services to build a solution that will be integrated with their app. The solution should be able to predict how long the cab ride will take.

Approach:
----------------------------------------------------------------
Given that the main focus of this work is related to the deployment in production of a model, i decided to use a mlflow server for tracking and registry of models. To deploy that, i used cloudformation templates and that's is beyond this repository. I made a python package to wrap all the code related with the work, and it's splitted in three main parts:

1. Data preparation:
Goes for the training data that it's in a storage bucket, the storage bucket it's received as a parameter in the execution. After that it removes the rows of the data that are beyond two standard deviations from mean. And removes the locations that are beyond New York city, given that the data is from that city.

2. Model training:
I decided to make a generic wrapper named train_model.py so the model definitions remains exclusevly in the taxi_model.py file. The model it's a scikit learn pipeline that receives the data in the same format as the training and test files, but it uses external functions for the feature engineering process. In that way after deploying the model in somewhere else, it will process the inputs in the exactly same way without other dependencies. 

    The purposed model transform the inputs into different distances, and splits datetime fields into different fields. Also transform every categorical variable to one hot encoded variables. I'm using a Gradient Boosing Regressor as the main part of the model.

3. Model API: 
After the model is trained and registred on the mlflow registry, the model it's available to be deployed. The api will get the model that corresponds to the deployment enviroment, given that mlflow allows to decide which model are going to the staging stage, or the production stage. In that way the api will use the model that it's authorized to be in production or staging.

The request schema is designed to remain equal even after another input is included in the model. It receives a list of features in this way:

    [
        {
            "feature": "feature A",
            "value": 0.00227878
        },
        {
            "feature": "feature B",
            "value": "2016-06-30 23:59:58"
        }
    ]


The mlflow server it's deployed in this url until the evaluation: http://Deplo-MLFLO-D88OEVSY616U-82eac8269ee913f7.elb.us-east-1.amazonaws.com



Testing, CI/CD, operationalization and remaining work
----------------------------------------------------------------

The next image shows the architecture of the solution. At this point it's not finished.

 ![Architecture](/architecture.png)

Regarding to testing, the package has the unit tests related with the columns transformations and for the api. 
About the operationalization, the design included an airflow dag to use this package in kubernetes pods using the airflow kubernetes operators.
When comes to CI/CD, it includes a gitlab manifest for the continuos integration process that it's working. The last step to deploy the API container in kubernetes or ECS remains inconcluded. 


Install
----------------------------------------------------------------

The repository has it's makefile for the main tasks in a personal enviroment. Make sure that you have prepared a virtual environment for the installation.

To install the package in a local environment please run the next command:

    $ make install

To run the tests of the package and check coverage, please run the next command:

    $ make tests

To build a wheel package, just type:

    $ make build

To execute the remaining tasks, please run the next command after installing the package. Please make sure that the aws credentials are configured propertly and that the enviroment variables **AWS_ACCESS_KEY_ID** and **AWS_SECRET_ACCESS_KEY** are setted up.

    $ make prepare_data

To execute the training model process, please run the next command

    $ make train_model

To execute the model api, run the next command:

    $ make model_api

after the flask server is running, you can send a post request with the next command:

    $ curl --header "Content-Type: application/json" \
        --request POST \
        --data '[{"feature":"vendor_id","value":1},
        {"feature":"pickup_datetime","value":"2016-06-30 23:59:58"},
        {"feature":"passenger_count","value":1},
        {"feature":"pickup_longitude","value":-73.988128662109375},
        {"feature":"pickup_latitude","value":40.732028961181641},
        {"feature":"dropoff_longitude","value":-73.99017333984375},
        {"feature":"dropoff_latitude","value":40.756679534912109},
        {"feature":"store_and_fwd_flag","value":"N"}]' \
        127.0.0.1:5000/taxi_trip_duration_inference

